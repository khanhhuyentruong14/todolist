/* eslint-disable prettier/prettier */

import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { list } from '../../constants/List';
import { TaskState } from '../types/task';

const initialState = list;

export const TaskSlice = createSlice({
    name: 'task',
    initialState,
    reducers: {
        add: (state: any, action: PayloadAction<TaskState>) => {
            state.push(action.payload);
        },
        remove: (state: any, action: PayloadAction<number>) => {
            const newState = state.filter((task: any) => task.id !== action.payload);
            return newState;
        },
        toggleComplete: (state: any, action: PayloadAction<number>) => {
            const newState = state.map((task: TaskState) =>
                task.id === action.payload ? { ...task, state: !task.state } : task
            );
            return newState;
        },
    },
});

export const { add, remove, toggleComplete } = TaskSlice.actions;

export default TaskSlice.reducer;
