/* eslint-disable prettier/prettier */

export interface TaskState {
    id: number,
    content: string,
    state: boolean,
}
