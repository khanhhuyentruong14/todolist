/* eslint-disable prettier/prettier */

import { combineReducers, configureStore } from '@reduxjs/toolkit';
import taskReducer from './slices/taskSlice';

const rootReducer = combineReducers({
    task: taskReducer,
    // more reducer
});

const store = configureStore({
    reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
