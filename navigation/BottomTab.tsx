/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-unstable-nested-components */
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen';
import { Image } from 'react-native';

const Tab = createBottomTabNavigator();
const BottomTab = () => {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Home',
          tabBarIcon: () => {
            return (
              <Image
                style={{ width: 30, height: 30 }}
                source={require('../assets/homeTab.png')}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Search"
        component={SearchScreen}
        options={{
          title: 'Search',
          tabBarIcon: () => {
            return (
              <Image
                style={{ width: 30, height: 30 }}
                source={require('../assets/searchTab.png')}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;