/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { View, Text, StyleSheet, Dimensions, Button, Image, Pressable } from 'react-native';
import React, { useState } from 'react';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import { themeColors } from '../theme';

interface Props {
    task: {
        id: number,
        content: string,
        state: boolean,
    },
    onDelete: any,
    toggleComplete: any,
}

const { width } = Dimensions.get('screen');
const TaskCard = ({ task, onDelete, toggleComplete }: Props) => {
    return (
        <View style={styles.container}>
            <BouncyCheckbox
                onPress={() => toggleComplete(task.id)}
                isChecked={task.state}
                fillColor={themeColors.activeButtonColor}
            />
            <Text
                style={{
                    textDecorationLine: task.state ? 'line-through' : 'none',
                    width: width * 0.6,
                    fontSize: 18,
                    textAlign: 'center',
                }}
            >
                {task.content}</Text>
            <Pressable onPress={() => onDelete(task.id)}>
                <Image
                    source={require('../assets/del.jpg')}
                    style={styles.icon}
                />
            </Pressable>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#fff',
        marginHorizontal: 20,
        marginVertical: 8,
        borderRadius: 10,
        elevation: 10,
        shadowColor: '#171717',
    },
    icon: {
        width: 20,
        height:20,
        resizeMode: 'contain',
    },
});

export default TaskCard;
