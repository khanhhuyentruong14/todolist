/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';
import React from 'react';
import { themeColors } from '../theme';

const width = Dimensions.get('window').width;
const HeaderHome = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Manage your time well</Text>
            <Image style={styles.image} source={require('../assets/File.png')} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height: 120,
        marginHorizontal: 20,
        marginTop: 50,
        marginBottom: 20,
        padding: 20,
        backgroundColor: themeColors.mainColor,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 25,
        elevation: 20,
        shadowColor: '#000',
    },
    title: {
        color: '#fff',
        fontSize: 22,
        fontWeight: '800',
        maxWidth: '80%',
    },
    image: {
        width: 60,
        height: 80,
    },
});

export default HeaderHome;