/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { View, Image, TextInput, StyleSheet, StatusBar, Dimensions, FlatList } from 'react-native';
import React, { useEffect, useState } from 'react';
import TaskCard from '../components/TaskCard';
import { useAppDispatch, useAppSelector } from '../redux/hook';
import { RootState } from '../redux/store';
import { remove, toggleComplete } from '../redux/slices/taskSlice';

const { height, width } = Dimensions.get('window');

const SearchScreen = () => {

  const [searchResults, setSearchResults] = useState<any>();
  const [text, onChangeText] = useState<string>('');
  const dispatch = useAppDispatch();
  const todoListRedx = useAppSelector((state: RootState) => state.task);

  const handleSearch = () => {
    setSearchResults(todoListRedx.filter((task: any) =>
      (task.content.toLowerCase().includes(text.toLowerCase()))
    ));
  };

  const handleDeleteTask = (id: number) => {
    dispatch(remove(id));
  };

  const handleToggleComplete = (id: number) => {
    dispatch(toggleComplete(id));
  };

  useEffect(
    () => {
      handleSearch();
    }, [text, todoListRedx]
  );

  return (
    <View>
      <StatusBar />
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          placeholder="Enter task..."
          onChangeText={onChangeText}
          value={text}
        />
      </View>
      <View>
        {text !== '' && searchResults.length > 0 ?
          <FlatList
            data={searchResults}
            renderItem={({ item }) => (
              <TaskCard
                key={item.id}
                task={item}
                onDelete={handleDeleteTask}
                toggleComplete={handleToggleComplete} />
            )}
          />
          :
          <View>
            <Image style={styles.noReasultImg} source={require('../assets/noResult.png')} />
          </View>
        }
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  textInput: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    height: 50,
    marginTop: 20,
    borderRadius: 50,
    paddingHorizontal: 20,
  },
  noReasultImg: {
    width: width,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

export default SearchScreen;
