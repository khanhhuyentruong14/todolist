/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { View, Text, FlatList, StyleSheet, Button, Modal, Dimensions, TextInput, ScrollView, Pressable, StatusBar } from 'react-native';
import React, { useState } from 'react';
import TaskCard from '../components/TaskCard';
import HeaderHome from '../components/HeaderHome';
import { themeColors } from '../theme';
import { add, remove, toggleComplete } from '../redux/slices/taskSlice';
import { RootState } from '../redux/store';
import { useAppDispatch, useAppSelector } from '../redux/hook';

const { height, width } = Dimensions.get('screen');
const HomeScreen = () => {
    const [isShowModal, setShowModel] = useState(false);
    const [text, onChangeText] = useState<string>('');
    const dispatch = useAppDispatch();
    const todoListRedx = useAppSelector((state: RootState) => state.task);

    const handleAddTask = () => {
        if (text !== '') {
            dispatch(add(
                {
                    id: (todoListRedx.length + 1) * Math.random(),
                    content: text,
                    state: false,
                },
            ));
        }
    };

    const handleDeleteTask = (id: number) => {
        dispatch(remove(id));
    };

    const handleToggleComplete = (id: number) => {
        dispatch(toggleComplete(id));
    };

    return (
        <ScrollView style={styles.container}>
            <StatusBar />
            <HeaderHome />
            <View style={{ minHeight: height }}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                    <Text style={styles.headerTitle}>Incomplete</Text>
                    <Pressable onPress={() => { setShowModel(true); }}>
                        <View>
                            <Text style={styles.addBtn}>+</Text>
                        </View>
                    </Pressable>
                </View>
                <View>
                    {todoListRedx.map((item) => (
                        !item?.state ? <TaskCard key={item.id} task={item} onDelete={handleDeleteTask} toggleComplete={handleToggleComplete} /> : ''
                    ))}
                </View>
                <View>
                    <Text style={styles.headerTitle}>Complete</Text>
                    {todoListRedx.map((item) => (
                        item?.state ? <TaskCard key={item.id} task={item} onDelete={handleDeleteTask} toggleComplete={handleToggleComplete} /> : ''
                    ))}
                </View>
            </View>

            <Modal
                animationType="slide"
                visible={isShowModal}
                transparent={true}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalInner}>
                        <Text style={styles.modalTitle}>Add a new task</Text>
                        <TextInput
                            style={styles.modalInput}
                            onChangeText={onChangeText}
                            value={text}
                        />
                        <View style={styles.groupButton}>
                            <View style={{ marginRight: 10 }}>
                                <Button
                                    title="ADD"
                                    color={themeColors.activeButtonColor}
                                    onPress={() => {
                                        handleAddTask();
                                        setShowModel(false);
                                        onChangeText('');
                                    }}
                                />
                            </View>
                            <View>
                                <Button
                                    title="Close"
                                    color={themeColors.disableButtonColor}
                                    onPress={() => {
                                        setShowModel(false);
                                        onChangeText('');
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'whitesmoke',
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
    headerTitle: {
        fontSize: 18,
        fontWeight: '600',
        marginVertical: 20,
        paddingHorizontal: 20,
    },
    modalContainer: {
        height: height,
        width: width,
        backgroundColor: 'rgba(225,225,225,0.8)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalInner: {
        width: width * 0.8,
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
    },
    modalTitle: {
        fontSize: 20,
        fontWeight: '600',
    },
    modalInput: {
        backgroundColor: 'rgba(0,0,0,0.1)',
        height: 40,
        marginTop: 20,
        borderRadius: 10,
        paddingHorizontal: 10,
    },
    groupButton: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 20,
    },
    addBtn: {
        color: '#fff',
        fontSize: 30,
        fontWeight: '400',
        backgroundColor: themeColors.mainColor,
        height: 40,
        width: 40,
        borderRadius: 50,
        textAlign: 'center',
        alignSelf: 'center',
        marginRight: 20,
    },
});


export default HomeScreen;
