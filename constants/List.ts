/* eslint-disable prettier/prettier */
export const list = [
    {
        id: 1,
        content: 'Sleeping',
        state: true,
    },
    {
        id: 2,
        content: 'Buy pens and books',
        state: false,
    },
    {
        id: 3,
        content: 'Cleaning house',
        state: true,
    },
    {
        id: 4,
        content: 'Learning English',
        state: false,
    },
    {
        id: 5,
        content: 'Reading a book',
        state: false,
    },
];
