/* eslint-disable prettier/prettier */
export const themeColors = {
  mainColor: '#53A6D8',
  activeButtonColor: '#47CACC',
  disableButtonColor: '#FE676E',
};
